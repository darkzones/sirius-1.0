import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: require('@/components/index').default
    },
    {
      path: '/syntax',
      component: require('@/components/syntax/Syntax').default
    },
    {
      path: '/dict',
      component: require('@/components/dict/Dict').default
    },
    {
      path: '/welcome',
      component: require('@/components/welcome/Welcome').default
    },
    {
      path: '/login',
      component: require('@/components/login/Login').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
