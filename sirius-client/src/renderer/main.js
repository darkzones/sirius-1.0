import Vue from 'vue'
import axios from 'axios'
import lodash from 'lodash'

import 'element-ui/lib/theme-chalk/index.css'
import 'font-awesome/css/font-awesome.css'

import App from './App'
import router from './router'
import store from './store'

import './global'
import './global.css'

Vue.use(require('vue-electron'))
Vue.use(require('element-ui'))

const instance = axios.create(global.AXIOS_CONFIG)
instance.interceptors.response.use(function(response) {
  response.success = response.data.result === 'success'
  return response
}, function(error) {
  return Promise.reject(error)
})

Vue.prototype.$http = instance
Vue.prototype._ = lodash
Vue.config.productionTip = false

new Vue({
  components: {
    App
  },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
