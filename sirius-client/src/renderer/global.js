// 左侧菜单初始折叠状态
global.IS_COLLSPAE = false

// axios异步请求配置
global.AXIOS_CONFIG = {
  baseURL: 'http://127.0.0.1:8080/client',
  headers: {'Sirius-Header': 'sirius'},
  timeout: 15000
}
