// initial state
const state = {
  navs: {
    left: '',
    top: ''
  }
}

// getters
const getters = {
  getNavs: state => state.navs
}

// actions
const actions = {
  setNavs ({ commit }, navs) {
    commit('setNavs', navs)
  }
}

// mutations
const mutations = {
  setNavs (state, navs) {
    state.navs = navs
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
