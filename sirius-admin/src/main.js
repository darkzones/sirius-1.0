// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'

import './global'

// 响应过滤
const axiosInstance = axios.create(global.AXIOS_CONFIG)
axiosInstance.interceptors.response.use(function (response) {
  response.success = response.data.result === 'success'
  response.sessionexpired = response.data.result === 'sessionexpired'
  if (response.sessionexpired) {
    router.push('/login')
  }
  return response
}, function (error) {
  return Promise.reject(error)
})

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$http = axiosInstance

// session超时过滤
router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    next()
  } else {
    Vue.prototype.$http.get('/login/checksession')
      .then((response) => {
        if (response.sessionexpired) {
          router.push('/login')
        } else {
          next()
        }
      })
      .catch((error) => {
        Vue.prototype.$alert('路由校验session失败', '提示信息', {
          confirmButtonText: '确定',
          type: 'error'
        })
        throw error
      })
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})
