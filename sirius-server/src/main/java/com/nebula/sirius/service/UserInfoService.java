package com.nebula.sirius.service;

import com.nebula.sirius.entity.UserInfoEntity;

/**
 * Created by Destiny on 03/30/2018 13:42.
 * Friday.
 */
public interface UserInfoService {

    void addUser(UserInfoEntity userInfo);
}
