package com.nebula.sirius.service.impl;

import com.nebula.sirius.entity.UserInfoEntity;
import com.nebula.sirius.repository.UserInfoRepository;
import com.nebula.sirius.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Destiny on 03/30/2018 13:43.
 * Friday.
 */
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoRepository userDao;

    @Override
    public void addUser(UserInfoEntity userInfo) {
        userDao.save(userInfo);
    }
}
