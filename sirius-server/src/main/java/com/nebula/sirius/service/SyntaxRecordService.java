package com.nebula.sirius.service;

import com.nebula.sirius.entity.SyntaxRecordEntity;
import org.springframework.data.domain.Page;

/**
 * Created by Destiny on 03/31/2018 09:30.
 * Saturday.
 */
public interface SyntaxRecordService {

    Page<SyntaxRecordEntity> getSyntax(int page, int size, String searchStr);

    void saveSyntax(SyntaxRecordEntity syntaxRecordEntity);

    void deleteSyntax(SyntaxRecordEntity syntaxRecordEntity);
}
