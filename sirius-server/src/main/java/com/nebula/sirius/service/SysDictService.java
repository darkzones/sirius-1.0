package com.nebula.sirius.service;

import com.nebula.sirius.entity.SysDictEntity;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by Destiny on 04/02/2018 19:52.
 * Monday.
 */
public interface SysDictService {

    List<SysDictEntity> getDict(String dictId);

    Page<SysDictEntity> getDict(int page, int size, String searchStr);

    void addDict(SysDictEntity sysDictEntity);

    void editDict(SysDictEntity sysDictEntity);

    void deleteDict(SysDictEntity sysDictEntity);
}
