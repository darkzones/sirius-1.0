package com.nebula.sirius.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * 类名称　: EnvConfigurer<br>
 * 功能描述: 读取配置内容（也可直接注入Environment类来读取参数配置）<br>
 * 所属包名: com.nebula.sirius.config<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/03 09:37:22<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Configuration
public class EnvConfigurer {

    @Autowired
    private Environment env;

    public int getServerPort() {
        return env.getProperty("server.port", Integer.class);
    }

    public String getCacheType() {
        return env.getProperty("spring.cache.type", String.class);
    }

}
