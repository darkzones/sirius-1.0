package com.nebula.sirius.config;

import com.nebula.sirius.Interceptor.ClientInterceptor;
import com.nebula.sirius.Interceptor.SessionInterceptor;
import com.nebula.sirius.Interceptor.WebInterceptor;
import com.nebula.sirius.kit.ConsKit;
import com.nebula.sirius.kit.HttpRequestLocalKit;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 类名称　: SiriusConfigurer<br>
 * 功能描述: 全局配置<br>
 * 所属包名: com.nebula.sirius.config<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/03/30 15:16:12<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Configuration
public class SiriusConfigurer implements WebMvcConfigurer, InitializingBean {

    @Autowired
    private HttpRequestLocalKit requestLocal;

    /**
     * 功能描述: 过滤器<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/13 16:43:28<br>
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SessionInterceptor(requestLocal)).addPathPatterns("/**");
        registry.addInterceptor(new WebInterceptor()).addPathPatterns(ConsKit.WEB_PERFIX + "/**");
        registry.addInterceptor(new ClientInterceptor()).addPathPatterns(ConsKit.CLIENT_PERFIX + "/**");
    }

    /**
     * 功能描述: 跨域访问配置，只允许客户端跨域<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/13 16:43:45<br>
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(ConsKit.CLIENT_PERFIX + "/**");
    }

    /**
     * 功能描述: 配置加载完后执行<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/13 16:43:58<br>
     */
    @Override
    public void afterPropertiesSet() {
    }
}
