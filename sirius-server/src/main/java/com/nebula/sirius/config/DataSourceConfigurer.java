package com.nebula.sirius.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

/**
 * 类名称　: DataSourceConfigurer<br>
 * 功能描述: 数据源配置<br>
 * 所属包名: com.nebula.sirius.config<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/03/30 15:15:56<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Configuration
public class DataSourceConfigurer {

    @Bean(name = "dataSource")
    public DataSource dataSource(Environment env) {
        HikariDataSource hikariDs = new HikariDataSource();
        hikariDs.setJdbcUrl(env.getProperty("spring.datasource.url"));
        hikariDs.setUsername(env.getProperty("spring.datasource.username"));
        hikariDs.setPassword(env.getProperty("spring.datasource.password"));
        hikariDs.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        hikariDs.setMaximumPoolSize(env.getProperty("spring.datasource.hikari.maximum-pool-size",
                Integer.class, 10));
        return hikariDs;
    }
}
