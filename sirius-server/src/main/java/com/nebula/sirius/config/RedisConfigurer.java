package com.nebula.sirius.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 功能描述: 利用redis做session共享，依赖redis缓存服务器<br>
 * 所属包名: com.nebula.sirius.config<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/10/2018 20:44 Tuesday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Configuration
@EnableCaching
//@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1200) // session超时时间，默认1800秒
public class RedisConfigurer {
}
