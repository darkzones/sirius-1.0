package com.nebula.sirius.message;

import org.springframework.stereotype.Service;

/**
 * Created by Destiny on 03/31/2018 10:05.
 * Saturday.
 */
@Service
public class SiriusResponse {

    private SiriusResult result = SiriusResult.unknown;

    private String resultMessage;

    private Object resultData;

    public SiriusResult getResult() {
        return result;
    }

    public void setResult(SiriusResult result) {
        this.result = result;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Object getResultData() {
        return resultData;
    }

    public void setResultData(Object resultData) {
        this.resultData = resultData;
    }
}
