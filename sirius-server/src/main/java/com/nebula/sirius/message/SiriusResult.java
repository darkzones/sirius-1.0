package com.nebula.sirius.message;

/**
 * 功能描述: <br>
 * 所属包名: com.nebula.sirius.message<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/01 12:14:15<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public enum SiriusResult {

    success,
    failed,
    sessionexpired,
    unknown
}
