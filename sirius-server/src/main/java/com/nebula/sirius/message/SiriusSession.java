package com.nebula.sirius.message;

import java.io.Serializable;

/**
 * 功能描述: <br>
 * 所属包名: com.nebula.sirius.message<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/10/2018 21:37 Tuesday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class SiriusSession implements Serializable {

    private String userId;
    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "SiriusSession{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
