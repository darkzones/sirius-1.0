package com.nebula.sirius.init;

import com.nebula.sirius.config.EnvConfigurer;
import com.nebula.sirius.init.info.StartupInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by Destiny on 04/02/2018 20:55.
 * Monday.
 */
@Component
@Order(value = 1)
public class GlobalInitRunner implements ApplicationRunner {

    @Autowired
    private StartupInfoService startInfo;

    @Autowired
    private EnvConfigurer env;

    @Override
    public void run(ApplicationArguments args) {

        System.out.println("Sirius server start on port " + env.getServerPort());
        startInfo.startInfo();

    }
}
