package com.nebula.sirius.init.info;

import com.nebula.sirius.config.EnvConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by Destiny on 04/04/2018 12:30.
 * Wednesday.
 */
@Service
@Profile("docker")
public class DockerStartupInfoServiceImpl implements StartupInfoService {

    @Autowired
    private EnvConfigurer env;

    @Override
    public void startInfo() {
        System.out.print("Docker configuration active. ");
        System.out.println("Cache type is " + env.getCacheType().toUpperCase());
    }
}
