package com.nebula.sirius.init;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by Destiny on 04/02/2018 20:55.
 * Monday.
 */
@Component
@Order(value = 2)
public class DictInitRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {

    }
}
