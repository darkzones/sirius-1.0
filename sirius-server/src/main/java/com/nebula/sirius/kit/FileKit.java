package com.nebula.sirius.kit;

import java.io.*;

/**
 * 功能描述: 文件处理类<br>
 * 所属包名: com.tfrunning.toolkit<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2017/12/29 03:05:40<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class FileKit {

    public static void delete(File file) {
        if (file != null && file.exists()) {
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                File files[] = file.listFiles();
                if (files != null) {
                    for (File file1 : files) {
                        delete(file1);
                    }
                }
                file.delete();
            }
        }
    }

    public static String getFileExtension(String fileFullName) {
        if (StrKit.isBlank(fileFullName)) {
            throw new RuntimeException("fileFullName is empty");
        }
        return getFileExtension(new File(fileFullName));
    }

    public static String getFileExtension(File file) {
        if (null == file) {
            throw new NullPointerException();
        }
        String fileName = file.getName();
        int dotIdx = fileName.lastIndexOf('.');
        return (dotIdx == -1) ? "" : fileName.substring(dotIdx + 1);
    }

    /**
     * 功能描述: 将字符串写入文件<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2017/12/29 10:27:46<br>
     */
    public static void writeFile(String fullPath, String fileName, String fileContent) {
        try {

            File classFile = new File(fullPath + fileName);
            if (!classFile.exists()) {
                classFile.createNewFile();
            }

            FileWriter writer = new FileWriter(classFile);
            writer.write(fileContent);
            writer.close();

        } catch (IOException e) {
            throw new RuntimeException("写入文件失败，" + e);
        }
    }

    /**
     * 功能描述: 文件末尾追加字符串<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2017/12/29 10:27:31<br>
     */
    public static void appendFile(String filePath, String content) {

        try {

            // 打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件
            FileWriter writer = new FileWriter(filePath, true);
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException("写入文件失败，" + e);
        }
    }

    /**
     * 功能描述: 将文件内容转换成byte数组<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/03/08 12:20:03<br>
     */
    private byte[] getBytes(String filePath) {

        byte[] buffer = null;

        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("获取文件数组异常，" + e.getMessage(), e);
        }

        return buffer;
    }

    /**
     * 功能描述: 移动文件到新的文件夹<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/03/14 16:54:28<br>
     */
    public static void moveToFolder(String source, String fileName, String target) {
        String sourcePath = source + File.separator + fileName;
        String targetPath = target + File.separator;
        try {
            File sourceFile = new File(sourcePath);
            File tmpFile = new File(targetPath); // 获取文件夹路径
            if (!tmpFile.exists()) {
                tmpFile.mkdirs();
            }
            if (!sourceFile.renameTo(new File(targetPath + sourceFile.getName()))) {
                throw new RuntimeException("移动文件失败");
            }
        } catch (Exception e) {
            throw new RuntimeException("移动文件异常");
        }
    }
}
