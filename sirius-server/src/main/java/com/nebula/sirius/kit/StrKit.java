package com.nebula.sirius.kit;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 功能描述: 字符串处理类<br>
 * 所属包名: com.tfrunning.core.kit<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2017/01/05 13:56:27<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class StrKit {

    /**
     * 首字母变小写
     */
    public static String firstCharToLowerCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'A' && firstChar <= 'Z') {
            char[] arr = str.toCharArray();
            arr[0] += ('a' - 'A');
            return new String(arr);
        }
        return str;
    }

    /**
     * 首字母变大写
     */
    public static String firstCharToUpperCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'a' && firstChar <= 'z') {
            char[] arr = str.toCharArray();
            arr[0] -= ('a' - 'A');
            return new String(arr);
        }
        return str;
    }

    /**
     * 字符串为 null 或者为  "" 时返回 true
     */
    public static boolean isBlank(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * 字符串不为 null 而且不为  "" 时返回 true
     */
    public static boolean notBlank(String str) {
        return str != null && !"".equals(str.trim());
    }

    public static boolean notBlank(String... strings) {
        if (strings == null) {
            return false;
        }
        for (String str : strings) {
            if (str == null || "".equals(str.trim())) {
                return false;
            }
        }
        return true;
    }

    public static boolean notNull(Object... paras) {
        if (paras == null) {
            return false;
        }
        for (Object obj : paras) {
            if (obj == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * 将带有 “_” 的字符串转换成驼峰命名规则的字符串
     *
     * @param stringWithUnderline _str
     * @return camelCaseStr
     */
    public static String toCamelCase(String stringWithUnderline) {

        stringWithUnderline = stringWithUnderline.toLowerCase();

        if (stringWithUnderline.indexOf('_') == -1) {
            return stringWithUnderline;
        }

        char[] fromArray = stringWithUnderline.toCharArray();
        char[] toArray = new char[fromArray.length];
        int j = 0;
        for (int i = 0; i < fromArray.length; i++) {
            if (fromArray[i] == '_') {
                // 当前字符为下划线时，将指针后移一位，将紧随下划线后面一个字符转成大写并存放
                i++;
                if (i < fromArray.length) {
                    toArray[j++] = Character.toUpperCase(fromArray[i]);
                }
            } else {
                toArray[j++] = fromArray[i];
            }
        }
        return new String(toArray, 0, j);
    }

    public static String join(String[] stringArray) {
        StringBuilder sb = new StringBuilder();
        for (String s : stringArray) {
            sb.append(s);
        }
        return sb.toString();
    }

    public static String join(String[] stringArray, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stringArray.length; i++) {
            if (i > 0) {
                sb.append(separator);
            }
            sb.append(stringArray[i]);
        }
        return sb.toString();
    }

    /**
     * 获取当前时间字符串
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(currentTime);
    }

    /**
     * 获取当前时间字符串，自定义格式化
     */
    public static String getStringDate(String format) {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(currentTime);
    }

    /**
     * 截取末尾逗号
     * @param oldstr 原字符串
     * @param comma 截取标识
     * @return 将末尾的截取标识截掉的字符串
     */
    public static String toUncommaStr(String oldstr, String comma) {
        if(oldstr.endsWith(comma)) {
            oldstr = oldstr.substring(0, oldstr.length() - 1);
        }
        return oldstr;
    }

    /**
     * 将字符串变成sql in条件式
     * @param oldstr 原字符串
     * @return in字符串
     */
    public static String toSqlinStr(String oldstr) {

        StringBuilder newstr = new StringBuilder();

        if (oldstr.contains(",")) {
            String[] ins = oldstr.split(",");
            for (String s : ins) {
                newstr.append("'").append(s).append("',");
            }
        } else {
            newstr.append("'").append(oldstr).append("'");
        }

        return toUncommaStr(newstr.toString(), ",");
    }

    public static List<String> toForStr(String oldstr) {

        List<String> strList = new ArrayList<>();

        String [] strs = oldstr.split(",");
        Collections.addAll(strList, strs);
        return strList;
    }

    public static boolean isEmpty(String str) {
    	return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return str != null && str.length() > 0;
    }

    /**
     * 功能描述: 获取当前时间前1天的时间<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2017/05/19 09:59:24<br>
     */
    public static String getBeforeDay(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();

        SimpleDateFormat time=new SimpleDateFormat("yyyyMMdd HH:mm:ss");

        return time.format(date);
    }

    /**
     * 功能描述: 获取当前时间前X天的时间<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2017/12/13 15:50:10<br>
     */
    public static String getBeforeDay(Date date, int dayStep) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -dayStep);
        date = calendar.getTime();

        SimpleDateFormat time=new SimpleDateFormat("yyyyMMdd HH:mm:ss");

        return time.format(date);
    }

}
