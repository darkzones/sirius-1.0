package com.nebula.sirius.kit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 功能描述: 保留用户会话，以方便在业务代码任何地方调用<br>
 * 所属包名: com.nebula.sirius.kit<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/13 17:00:40<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Component
public class HttpRequestLocalKit {

    @Autowired
    private HttpKit httpKit;

    private static final ThreadLocal<HttpServletRequest> requests = ThreadLocal.withInitial(() -> null);

    public void set(HttpServletRequest request) {
        requests.set(request);
    }

    public Object getSessionValue(String attr) {
        return requests.get().getSession().getAttribute(attr);
    }

    public void setSessionValue(String attr, Object obj) {
        requests.get().getSession().setAttribute(attr, obj);
    }

    public Object getRequestValue(String attr) {
        return requests.get().getAttribute(attr);
    }

    public String getRequestURI() {
        return requests.get().getRequestURI();
    }

    public String getRequestIP() {
        return httpKit.getIpAddr(requests.get());
    }

}
