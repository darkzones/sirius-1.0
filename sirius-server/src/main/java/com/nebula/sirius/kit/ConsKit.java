package com.nebula.sirius.kit;

/**
 * 类名称　: ConsKit<br>
 * 功能描述: 系统常量定义<br>
 * 所属包名: com.nebula.sirius.kit<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/03 10:38:27<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class ConsKit {

    public static final String SERVICE_NAME = "Sirius";

    public static final String USER_SESSION = "userSession";

    public static final String LOGIN_PATH = "/login";
    public static final String ERROR_PATH = "/error";
    public static final String INDEX_PATH = "/index";

    public static final String WEB_PERFIX = "/web";
    public static final String CLIENT_PERFIX = "/client";
}
