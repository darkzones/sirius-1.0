package com.nebula.sirius.controller.client;

import com.nebula.sirius.entity.SysDictEntity;
import com.nebula.sirius.kit.ConsKit;
import com.nebula.sirius.message.SiriusResponse;
import com.nebula.sirius.message.SiriusResult;
import com.nebula.sirius.service.SysDictService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 功能描述: client端数据字典访问<br>
 * 所属包名: com.nebula.sirius.controller.client<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/02 23:19:39<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
@RequestMapping(value = ConsKit.CLIENT_PERFIX + "/dict")
public class SysDictController {

    private static final Logger logger = LoggerFactory.getLogger(SysDictController.class);

    @Autowired
    SysDictService sysDictService;

    @RequestMapping(value = "/{dictId}")
    public SiriusResponse index(@PathVariable("dictId") String dictId) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            siriusResponse.setResultData(sysDictService.getDict(dictId));
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("获取数据字典异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping
    public SiriusResponse index(@RequestParam int page, @RequestParam int limit, @RequestParam String searchStr) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            siriusResponse.setResultData(sysDictService.getDict(page, limit, searchStr));
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("获取数据字典异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping(value = "/add")
    public SiriusResponse add(@RequestBody SysDictEntity sysDictEntity) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            sysDictService.addDict(sysDictEntity);
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("保存数据异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping(value = "/edit")
    public SiriusResponse edit(@RequestBody SysDictEntity sysDictEntity) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            sysDictService.editDict(sysDictEntity);
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("保存数据异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping(value = "/delete")
    public SiriusResponse delete(@RequestBody SysDictEntity sysDictEntity) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            sysDictService.deleteDict(sysDictEntity);
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("删除数据异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }
}
