package com.nebula.sirius.controller.web;

import com.nebula.sirius.annotation.Function;
import com.nebula.sirius.entity.UserInfoEntity;
import com.nebula.sirius.kit.ConsKit;
import com.nebula.sirius.message.SiriusSession;
import com.nebula.sirius.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;

/**
 * 功能描述: web端访问首页<br>
 * 所属包名: com.nebula.sirius.controller.web<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/11 23:19:18<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
@RequestMapping(value = ConsKit.WEB_PERFIX + "/index")
public class IndexController {

    @Autowired
    UserInfoService userInfoService;

    @RequestMapping
    @Function("index")
    public String index(HttpServletRequest request) {
        System.out.println("sessionId:" + request.getSession().getId());

        SiriusSession siriusSession = (SiriusSession) request.getSession().getAttribute(ConsKit.USER_SESSION);

        return String.format("Hello %s(%s), your sessionId is: %s", siriusSession.getUserName(),
                siriusSession.getUserId(), request.getSession().getId());
    }

    @RequestMapping(value = "/get")
    @Function("index.get")
    public HashMap<String, Object> get(@RequestParam String name) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", "hello world");
        map.put("name", name);
        return map;
    }

    @RequestMapping(value = "/get/{id}/{name}")
    @Function("index.get.id.name")
    public UserInfoEntity getUser(@PathVariable String id, @PathVariable String name) {
        UserInfoEntity user = new UserInfoEntity();
        user.setUserId(id);
        user.setUserName(name);
        user.setCreateTime(new Date());
        return user;
    }

}
