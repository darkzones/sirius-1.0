package com.nebula.sirius.controller.client;

import com.nebula.sirius.entity.SyntaxRecordEntity;
import com.nebula.sirius.kit.ConsKit;
import com.nebula.sirius.message.SiriusResponse;
import com.nebula.sirius.message.SiriusResult;
import com.nebula.sirius.service.SyntaxRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述: client端语法访问<br>
 * 所属包名: com.nebula.sirius.controller.client<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/04/11 23:20:13<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
@RequestMapping(value = ConsKit.CLIENT_PERFIX + "/syntax")
public class SyntaxRecordController {

    private static final Logger logger = LoggerFactory.getLogger(SyntaxRecordController.class);

    @Autowired
    SyntaxRecordService syntaxRecordService;

    @RequestMapping
    public SiriusResponse index(@RequestParam int page, @RequestParam int limit, @RequestParam String searchStr) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            siriusResponse.setResultData(syntaxRecordService.getSyntax(page, limit, searchStr));
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("获取列表异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping(value = "/save")
    public SiriusResponse save(@RequestBody SyntaxRecordEntity syntaxRecordEntity) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            syntaxRecordService.saveSyntax(syntaxRecordEntity);
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("保存数据异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }

    @RequestMapping(value = "/delete")
    public SiriusResponse delete(@RequestBody SyntaxRecordEntity syntaxRecordEntity) {

        SiriusResponse siriusResponse = new SiriusResponse();

        try {
            siriusResponse.setResult(SiriusResult.success);
            syntaxRecordService.deleteSyntax(syntaxRecordEntity);
        } catch (Exception e) {
            siriusResponse.setResult(SiriusResult.failed);
            siriusResponse.setResultMessage("删除数据异常，" + e.getMessage());
            logger.error(siriusResponse.getResultMessage(), e);
        }

        return siriusResponse;
    }
}
