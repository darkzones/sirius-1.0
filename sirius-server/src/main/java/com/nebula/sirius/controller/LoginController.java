package com.nebula.sirius.controller;

import com.nebula.sirius.entity.UserInfoEntity;
import com.nebula.sirius.kit.ConsKit;
import com.nebula.sirius.kit.HttpRequestLocalKit;
import com.nebula.sirius.message.SiriusResponse;
import com.nebula.sirius.message.SiriusResult;
import com.nebula.sirius.message.SiriusSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述: <br>
 * 所属包名: com.nebula.sirius.controller<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/10/2018 21:41 Tuesday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
public class LoginController {

    @Autowired
    private HttpRequestLocalKit requestLocal;

    /**
     * 功能描述: web端登陆<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/11 23:18:52<br>
     */
    @PostMapping(ConsKit.WEB_PERFIX + "/login")
    public SiriusResponse webLogin(@RequestBody UserInfoEntity userInfoEntity, HttpServletRequest request) {

        SiriusResponse siriusResponse = new SiriusResponse();

        SiriusSession siriusSession = new SiriusSession();
        siriusSession.setUserId(userInfoEntity.getUserId());
        siriusSession.setUserName(userInfoEntity.getUserName());

        // TODO 数据库验证登录信息

        requestLocal.setSessionValue(ConsKit.USER_SESSION, siriusSession);

        siriusResponse.setResult(SiriusResult.success);

        return siriusResponse;
    }

    /**
     * 功能描述: web端session验证<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/15 23:18:52<br>
     */
    @GetMapping(ConsKit.WEB_PERFIX + "/login/checksession")
    public SiriusResponse webCheckSession(HttpServletRequest request) {

        SiriusResponse siriusResponse = new SiriusResponse();

        if(null == requestLocal.getSessionValue(ConsKit.USER_SESSION)) {
            siriusResponse.setResult(SiriusResult.sessionexpired);
        } else {
            siriusResponse.setResult(SiriusResult.success);
        }

        return siriusResponse;
    }

    /**
     * 功能描述: web端注销<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/15 23:18:52<br>
     */
    @GetMapping(ConsKit.WEB_PERFIX + "/logout")
    public SiriusResponse webLogout(HttpServletRequest request) {

        SiriusResponse siriusResponse = new SiriusResponse();
        request.getSession().invalidate();
        siriusResponse.setResult(SiriusResult.success);

        return siriusResponse;
    }

    /**
     * 功能描述: client端登陆<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/15 11:09:47<br>
     */
    @GetMapping(ConsKit.CLIENT_PERFIX + "/login/{userId}/{userName}")
    public SiriusResponse clientLogin(@PathVariable String userId, @PathVariable String userName) {

        SiriusResponse siriusResponse = new SiriusResponse();
        siriusResponse.setResult(SiriusResult.success);

        return siriusResponse;
    }

}
