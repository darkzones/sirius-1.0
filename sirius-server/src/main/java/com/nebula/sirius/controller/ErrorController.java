package com.nebula.sirius.controller;

import com.nebula.sirius.kit.ConsKit;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 功能描述: 通用错误处理<br>
 * 所属包名: com.nebula.sirius.controller<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/03/30 14:35:00<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Controller
public class ErrorController extends AbstractErrorController {

    public ErrorController() {
        super(new DefaultErrorAttributes());
    }

    /**
     * 功能描述: 所有404错误页面统一返回vue首页index.html，由前端vue路由决定页面表现。此举相当于禁用springboot路由<br>
     * 创建人　: 白剑<br>
     * 创建时间: 2018/04/14 16:02:34<br>
     */
    @Override
    @RequestMapping(ConsKit.ERROR_PATH)
    public String getErrorPath() {
        return ConsKit.INDEX_PATH;
    }
}
