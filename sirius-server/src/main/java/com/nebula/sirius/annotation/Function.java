package com.nebula.sirius.annotation;

import java.lang.annotation.*;

/**
 * 功能描述: 方法注解，通过aop来判断标注此注解的方法是否可以被访问，权限控制<br>
 * 所属包名: com.nebula.sirius.annotation<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/13/2018 14:34 Friday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Function {

    String value();

    String name() default "";
}
