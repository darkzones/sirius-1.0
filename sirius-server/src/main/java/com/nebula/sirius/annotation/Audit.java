package com.nebula.sirius.annotation;

import java.lang.annotation.*;

/**
 * 功能描述: 审计注解<br>
 * 所属包名: com.nebula.sirius.annotation<br>
 * 创建人　: 白剑<br>
 * 创建时间: 06/23/2018 17:55 Saturday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Audit {

    String value();
}
