package com.nebula.sirius.Interceptor;

import com.nebula.sirius.kit.HttpRequestLocalKit;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述: <br>
 * 所属包名: com.nebula.sirius.Interceptor<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/13/2018 16:46 Friday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class SessionInterceptor implements HandlerInterceptor {

    private HttpRequestLocalKit requestLocal;

    public SessionInterceptor(HttpRequestLocalKit requestLocal) {
        this.requestLocal = requestLocal;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        requestLocal.set(request);

        return true;
    }
}
