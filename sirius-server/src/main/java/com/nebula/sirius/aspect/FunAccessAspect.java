package com.nebula.sirius.aspect;

import com.nebula.sirius.annotation.Function;
import com.nebula.sirius.kit.HttpRequestLocalKit;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 功能描述: 权限/审计 拦截器<br>
 * 所属包名: com.nebula.sirius.aspect<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/13/2018 14:46 Friday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Aspect
@Component
public class FunAccessAspect {

    @Autowired
    private HttpRequestLocalKit requestLocal;

    @Pointcut("@annotation(com.nebula.sirius.annotation.Function)")
    public void access() {
    }

    @Before("access()")
    public void before(JoinPoint joinPoint) {
    }

    @Around("@annotation(function)")
    public Object around(ProceedingJoinPoint pjp, Function function) {

        try {

            // 根据 function.value() 判断登录用户是否有方法访问权限
            // System.out.println(requestLocal.getSessionValue(ConsKit.USER_SESSION));
            // System.out.println(requestLocal.getRequestIP());

            MethodSignature ms = (MethodSignature) pjp.getSignature();
            Method m = ms.getMethod();
            // System.out.println(m.getName());

            // 访问成功审计

            return pjp.proceed();

        } catch (Throwable throwable) {

            throwable.printStackTrace();

            // 访问失败审计

            return null;
        }
    }
}
