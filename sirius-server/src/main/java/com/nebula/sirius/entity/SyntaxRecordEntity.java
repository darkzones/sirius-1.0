package com.nebula.sirius.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Destiny on 03/31/2018 09:20.
 * Saturday.
 */
@Entity
@Table(name = "syntax_record", schema = "sirius")
public class SyntaxRecordEntity implements Serializable {
    private Integer syntaxId;
    private String syntaxType;
    private String syntaxContent;
    private String syntaxCreateTime;

    @Id
    @Column(name = "syntax_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getSyntaxId() {
        return syntaxId;
    }

    public void setSyntaxId(Integer syntaxId) {
        this.syntaxId = syntaxId;
    }

    @Basic
    @Column(name = "syntax_type")
    public String getSyntaxType() {
        return syntaxType;
    }

    public void setSyntaxType(String syntaxType) {
        this.syntaxType = syntaxType;
    }

    @Basic
    @Column(name = "syntax_content")
    public String getSyntaxContent() {
        return syntaxContent;
    }

    public void setSyntaxContent(String syntaxContent) {
        this.syntaxContent = syntaxContent;
    }

    @Basic
    @Column(name = "syntax_create_time")
    public String getSyntaxCreateTime() {
        return syntaxCreateTime;
    }

    public void setSyntaxCreateTime(String syntaxCreateTime) {
        this.syntaxCreateTime = syntaxCreateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SyntaxRecordEntity that = (SyntaxRecordEntity) o;
        return syntaxId.equals(that.syntaxId) &&
                Objects.equals(syntaxType, that.syntaxType) &&
                Objects.equals(syntaxContent, that.syntaxContent) &&
                Objects.equals(syntaxCreateTime, that.syntaxCreateTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(syntaxId, syntaxType, syntaxContent, syntaxCreateTime);
    }
}
