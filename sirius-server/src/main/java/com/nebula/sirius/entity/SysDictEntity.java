package com.nebula.sirius.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Destiny on 04/02/2018 19:47.
 * Monday.
 */
@Entity
@Table(name = "sys_dict", schema = "sirius")
@IdClass(SysDictEntityPK.class)
public class SysDictEntity implements Serializable {
    private String dictId;
    private String dictName;
    private Integer dictSeq;
    private String dictKey;
    private String dictValue;
    private String dictSts;

    @Id
    @Column(name = "dict_id")
    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    @Basic
    @Column(name = "dict_name")
    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    @Basic
    @Column(name = "dict_seq")
    public Integer getDictSeq() {
        return dictSeq;
    }

    public void setDictSeq(Integer dictSeq) {
        this.dictSeq = dictSeq;
    }

    @Id
    @Column(name = "dict_key")
    public String getDictKey() {
        return dictKey;
    }

    public void setDictKey(String dictKey) {
        this.dictKey = dictKey;
    }

    @Basic
    @Column(name = "dict_value")
    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    @Basic
    @Column(name = "dict_sts")
    public String getDictSts() {
        return dictSts;
    }

    public void setDictSts(String dictSts) {
        this.dictSts = dictSts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysDictEntity that = (SysDictEntity) o;
        return Objects.equals(dictId, that.dictId) &&
                Objects.equals(dictName, that.dictName) &&
                Objects.equals(dictSeq, that.dictSeq) &&
                Objects.equals(dictKey, that.dictKey) &&
                Objects.equals(dictValue, that.dictValue) &&
                Objects.equals(dictSts, that.dictSts);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dictId, dictName, dictSeq, dictKey, dictValue, dictSts);
    }
}
