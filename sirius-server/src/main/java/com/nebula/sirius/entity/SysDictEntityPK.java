package com.nebula.sirius.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Destiny on 04/02/2018 19:47.
 * Monday.
 */
public class SysDictEntityPK implements Serializable {
    private String dictId;
    private String dictKey;

    @Column(name = "dict_id")
    @Id
    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    @Column(name = "dict_key")
    @Id
    public String getDictKey() {
        return dictKey;
    }

    public void setDictKey(String dictKey) {
        this.dictKey = dictKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysDictEntityPK that = (SysDictEntityPK) o;
        return Objects.equals(dictId, that.dictId) &&
                Objects.equals(dictKey, that.dictKey);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dictId, dictKey);
    }
}
