package com.nebula.sirius.repository;

import com.nebula.sirius.entity.SysDictEntity;
import com.nebula.sirius.entity.SysDictEntityPK;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by Destiny on 04/02/2018 19:50.
 * Monday.
 */
public interface SysDictRepository extends JpaRepository<SysDictEntity, SysDictEntityPK>,
        JpaSpecificationExecutor {

    List<SysDictEntity> findAllByDictId(String dictId);

    List<SysDictEntity> findAllByDictId(String dictId, Sort sort);
}
