package com.nebula.sirius.repository;

import com.nebula.sirius.entity.UserInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Destiny on 03/30/2018 13:39.
 * Friday.
 */
public interface UserInfoRepository extends JpaRepository<UserInfoEntity, String> {

}
