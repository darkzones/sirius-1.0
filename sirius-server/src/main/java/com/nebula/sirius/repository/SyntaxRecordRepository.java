package com.nebula.sirius.repository;

import com.nebula.sirius.entity.SyntaxRecordEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Destiny on 03/31/2018 09:25.
 * Saturday.
 */
public interface SyntaxRecordRepository extends JpaRepository<SyntaxRecordEntity, Integer>,
        JpaSpecificationExecutor {

    Page<SyntaxRecordEntity> findBySyntaxTypeAndSyntaxContentLike(String type, String content, Pageable pageable);

    Page<SyntaxRecordEntity> findBySyntaxContentLike(String content, Pageable pageable);

    Page<SyntaxRecordEntity> findBySyntaxType(String type, Pageable pageable);
}
