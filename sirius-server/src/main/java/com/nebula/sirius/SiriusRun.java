package com.nebula.sirius;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 类名称　: SiriusRun<br>
 * 功能描述: 启动服务<br>
 * 所属包名: com.nebula.sirius<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/03/29 16:42:39<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@SpringBootApplication
@ServletComponentScan
@EnableCaching
@EnableScheduling
public class SiriusRun {

    public static void main(String[] args) {
        SpringApplication.run(SiriusRun.class, args);
    }
}
