package com.nebula.sirius.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能描述: 定时任务<br>
 * 所属包名: com.nebula.sirius.task<br>
 * 创建人　: 白剑<br>
 * 创建时间: 04/14/2018 18:19 Saturday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Component
public class SiriusTask {

    private static final ThreadLocal<SimpleDateFormat> dataFormat =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    private int startMinius = 0;
    private static final String initTime = dataFormat.get().format(new Date());

    @Scheduled(fixedRate = 60000)
    public void siriusTime() {
        System.out.println(String.format("--- you have running sirius-server for %s minutes since %s. ---",
                startMinius++, initTime));
    }

    private static final long breakInterval = 60 * 60 * 1000;

    @Scheduled(initialDelay = breakInterval, fixedDelay = breakInterval)
    public void breakTime() {
        System.out.println("--- 休息，休息一下~ ^_~ ---");
    }

}
