package com.nebula.sirius;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Destiny on 04/04/2018 17:25.
 * Wednesday.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SiriusRunTests {

    @Test
    public void contextLoads() {
    }
}
