# sirius-1.0

后端基于spring boot 2.0.0构建，前端基于vue技术栈构建，客户端基于electron-vue构建。
可以基于此项目开发前后端分离的web项目，也可以开发client/server项目。
并且由于electron的支持，可以将开发的项目以web方式发布，也可以以c/s方式发布

## sirius-admin

- vue-cli 构建
- 前后端分离，后端只提供rest服务，路由前端来控制
- build 部署包直接生成到sirius-server/resources/static/dist目录下
- 若使用nginx，可以不发布到server下，直接用nginx指向前端静态资源即可
- docker容器化部署

## sirius-client

- vue
- element-ui
- lodash
- electron
- electron-vue
- webpack
- nodejs
- es6

## sirius-server

- spring boot
- maven
- redis
- mysql
- fastjson
- docker